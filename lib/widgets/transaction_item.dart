import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../moduls/transaction.dart';

class TransactionItem extends StatefulWidget {
  const TransactionItem({
    Key key,
    @required Transaction userTransaction,
    @required this.deleteTransaction,
  })  : _userTransaction = userTransaction,
        super(key: key);

  final Transaction _userTransaction;
  final Function deleteTransaction;

  @override
  _TransactionItemState createState() => _TransactionItemState();
}

class _TransactionItemState extends State<TransactionItem> {


  Color _bgColor;
  @override
  void initState() {
    const avaliableColors = [
      Colors.red,
      Colors.black,
      Colors.purple,
      Colors.blue,
    ];

    _bgColor = avaliableColors[Random().nextInt(4)];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 5,
      ),
      elevation: 5,
      child: ListTile(
        leading: CircleAvatar(
          radius: 30,
          backgroundColor: _bgColor,
          child: Padding(
            padding: const EdgeInsets.all(6),
            child: FittedBox(
                child: Text('\$${widget._userTransaction.amount.toString()}')),
          ),
        ),
        title: Text(widget._userTransaction.title,
            style: Theme.of(context).textTheme.title),
        subtitle: Text(DateFormat.yMMMd().format(widget._userTransaction.date)),
        trailing: MediaQuery.of(context).size.width > 460
            ? FlatButton.icon(
                onPressed: () =>
                    widget.deleteTransaction(widget._userTransaction.id),
                icon: const Icon(Icons.delete),
                label: const Text("delete!"),
                textColor: Theme.of(context).errorColor,
              )
            : IconButton(
                color: Theme.of(context).errorColor,
                icon: const Icon(Icons.delete),
                onPressed: () =>
                    widget.deleteTransaction(widget._userTransaction.id),
              ),
      ),
    );
  }
}
