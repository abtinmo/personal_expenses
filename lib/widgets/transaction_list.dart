import 'package:flutter/material.dart';
import './transaction_item.dart';
import '../moduls/transaction.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> _userTransactions;
  final Function deleteTransaction;

  TransactionList(this._userTransactions, this.deleteTransaction);

  @override
  Widget build(BuildContext context) {
    return _userTransactions.isEmpty
        ? LayoutBuilder(
            builder: (ctx, constrators) {
              return Column(
                children: <Widget>[
                  Text(
                    'No Transactions',
                    style: Theme.of(context).textTheme.title,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: constrators.maxHeight * 0.6,
                    child: Image.asset(
                      'assets/images/waiting.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              );
            },
          )
        : ListView(
            children: _userTransactions
                .map((tnx) => TransactionItem(
                      userTransaction: tnx,
                      deleteTransaction: deleteTransaction,
                      key: ValueKey(tnx.id),
                    ))
                .toList());
  }
}
